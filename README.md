## Using scorep to compile openfoam
- Scorep is a parallel performance measurement infrastucture which can help to gain insights into parallel performance of scientific codes
- Scorep7.1 is used here with OpenFoam-v2206
- First of all ensure that you have a working version of scorep available
- More information about scorep can be found here <https://www.vi-hps.org/projects/score-p/>
- Just change the files mentioned in this repo in your Openfoam version and you can compile openfoam using scorep
- Openfoam uses pstream for parallel communication
- MPI implementation is hidden under pstream
- Pstream implementation is availbable in `$WM_PROJECT_DIR/src/Pstream/mpi`. It makes use of MPI_AllReduce, MPI_Scatter, MPI_Gather, MPI_Bcast etc. functions.
- Use following flags after sourcing openfoam environment, to check the compiler paths and mpi paths
```bash
wmake -show-path-c
wmake -show-path-cxx
wmake -show-compile-c
wmake -show-compile-cxx
wmake -show-mpi-compile
wmake -show-mpi-link
```

### Follow these steps to compile your Openfoam version using scorep
- Copy the linux64ScorepGccNew folder from this repository to `$WM_PROJECT_DIR/wmake/rules/`
- Update the Makefile at `$WM_PROJECT_DIR/wmake/src/Makefile` according to this repo
- Update the bashrc `$WM_PROJECT_DIR/etc/bashrc` according to this repo
- Now openfoam is ready to be compiled with scorep
- Use the normal procedure `Allwmake -j` for compiling openfoam


